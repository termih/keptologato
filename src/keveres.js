function veletlen(felsoHatar) {
	return Math.floor(Math.random() * felsoHatar);
}

function lekerCserelendok() {
	//Tömb feltöltése:
	let tomb = Array.from(Array(15).keys());
	let elsoKepSzama = veletlen(15);
	tomb.splice(elsoKepSzama, 1);
	let masodikVeletlenSzam = veletlen(14);
	let masodikKepSzama = tomb[masodikVeletlenSzam];
	let cserelendok = new Array();
	cserelendok.push(elsoKepSzama);
	cserelendok.push(masodikKepSzama);
	return cserelendok;
}

function csere(elsoKepSzama, masodikKepSzama) {
	let kep1 = kepek.getElementsByTagName('img')[elsoKepSzama];	
	let kep2 = kepek.getElementsByTagName('img')[masodikKepSzama];
	let tmp = kep1.src;
	kep1.src = kep2.src;
	kep2.src = tmp;
}

function kever() {	
	for(let i=0;i<20; i++) {
		let cserelendok = lekerCserelendok();
		csere(cserelendok[0], cserelendok[1]);
	}
}
