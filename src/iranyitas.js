//megnézi, hogy üres-e képhely
function vizsgalUres(x, y) {
	let kepek = document.getElementById('kepek');
	let ures = true;
	for(let i=0; i<15; i++) {
		let kep = kepek.getElementsByTagName('img')[i];
		if (kep.style.left == x+'px' && kep.style.top == y+'px') {
			ures=false;
		}
	}
	return ures;
}

//100px esetén visszaadja: 100
function lekerErtek(keppont) {
	let keppontHossz = keppont.length;
	let keppontValue = Number(keppont.slice(0, keppontHossz-2));
	return keppontValue;
}

//megvizsgálja lent üres van-e, jó-e
function le_jo(kep) {
	let x = lekerErtek(kep.style.left);
	let y = lekerErtek(kep.style.top);
	
	let jo = false;
	if(y<300) {
		jo = vizsgalUres(x, y + 100);
	}
	return jo;
}

//megvizsgálja fent üres van-e, jó-e
function fel_jo(kep) {
	let x = lekerErtek(kep.style.left);
	let y = lekerErtek(kep.style.top);
	
	let jo = false;
	if(y>0) {
		jo = vizsgalUres(x, y - 100);
	}
	return jo;
}

//megvizsgálja jobbra üres van-e, jó-e
function jobb_jo(kep) {
	let x = lekerErtek(kep.style.left);
	let y = lekerErtek(kep.style.top);
	
	let jo = false;
	if(x<300) {
		jo = vizsgalUres(x+100, y);
	}
	return jo;
}

//megvizsgálja balra üres van-e, jó-e
function bal_jo(kep) {
	let x = lekerErtek(kep.style.left);
	let y = lekerErtek(kep.style.top);
	
	let jo = false;
	if(x>0) {
		jo = vizsgalUres(x-100, y);
	}
	return jo;
}

//mozgatás
function mozgat(kep, x, y) {
	kep.style.left = x + 'px';
	kep.style.top = y + 'px';	
}

//irány megállapítása
function iranyit(kep) {
	let x = lekerErtek(kep.style.left);
	let y = lekerErtek(kep.style.top);
	
	if(le_jo(kep)) {
		mozgat(kep, x, y + 100);
	}else if(fel_jo(kep)){
		mozgat(kep, x, y - 100);
	}else if(jobb_jo(kep)) {
		mozgat(kep, x+100, y);
	}else if(bal_jo(kep)) {
		mozgat(kep, x-100, y);
	}
}

/* beallitKeppont('100px', -50)  */
function beallitKeppont(keppont, ertek) {
	let keppontHossz = keppont.length;
	let keppontValue = Number(keppont.slice(0, keppontHossz-2));
	keppontValue += ertek;
	return keppontValue + 'px';
}
