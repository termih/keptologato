'use strict';

var eredetiKepNev = 'kep01.png';
var poziciok = [
	[0, 0, 'kep01_01'],	  [100, 0, 'kep01_02'],   [200, 0, 'kep01_03'],   [300, 0, 'kep01_04'],
	[0, 100, 'kep01_05'], [100, 100, 'kep01_06'], [200, 100, 'kep01_07'], [300, 100, 'kep01_08'],
	[0, 200, 'kep01_09'], [100, 200, 'kep01_10'], [200, 200, 'kep01_11'], [300, 200, 'kep01_12'],
	[0, 300, 'kep01_13'], [100, 300, 'kep01_14'], [200, 300, 'kep01_15'], [300, 300, undefined]		
];

// Képek feltöltése a poziciok tömbből
function elokeszitTologato() {
	let kepek = document.getElementById('kepek');
	for(let i=0; i<15; i++) {
		let kep = kepek.getElementsByTagName('img')[i];
		let xpos = poziciok[i][0];
		let ypos = poziciok[i][1];
		let x = xpos + "px";
		let y = ypos + "px";			
		kep.style.left = x;
		kep.style.top = y;
		kep.src='images/'+ poziciok[i][2] + '.png';
	}	
	let eredetiKepDoboz = document.getElementById('eredetiKepDoboz');
	let eredetiKep = eredetiKepDoboz.getElementsByTagName('img')[0];
	let kepUtvonal = 'images/' + eredetiKepNev;
	eredetiKep.src = kepUtvonal;
}


function esemenykezeles() {
	let kepek = document.getElementById('kepek');
	for(let i=0; i<15; i++) {
		let kep = kepek.getElementsByTagName('img')[i];
		kep.addEventListener('click', function(){
			iranyit(kep);
		});
	}
}


let keverGomb = document.getElementById('keverGomb');
keverGomb.addEventListener('click', function() {
	kever();
});

let mutatGomb = document.getElementById('mutatGomb');
mutatGomb.addEventListener('click', function() {
	let eredetiKepDoboz = document.getElementById('eredetiKepDoboz');
	if (eredetiKepDoboz.style.display=='none' || 
			eredetiKepDoboz.style.display=='') {
		eredetiKepDoboz.style.display = 'block';
	}else {
		eredetiKepDoboz.style.display = 'none';
	}	
});

elokeszitTologato();
esemenykezeles();

